# MilitaryMissions

To me it was always frustrating that missions from military outpost was hard coded in the Merchant script, or to say it in easy words: adding new missions to military outpost is not very nice in vanilla game. So I created a mod wich makes you able to add new missions to military outpost as plugins. That's what this mod basicly does.

As an example plugin this mod contains the old but gold modded mission ["Pirate Warlord" by Theoman02](http://www.avorion.net/forum/index.php/topic,781.0.html).

Also it is possible to add the mod [XsotanDreadnought](http://www.avorion.net/forum/index.php/topic,4325.msg22852.html) as a plugin for this mod.

## Installation

Extract contents of the Avorion directory in zip file into your local Avorion directory.

Add the following code to the very bottom of the file `data/scripts/entity/merchants/militaryoutpost.lua`:

    local s,e=pcall(require,'mods.MilitaryMissions.scripts.entity.merchants.militaryoutpost');if not s then print('Mod: MilitaryMissions, failed to extend militaryoutpost.lua!') print(e) end

### Optional: Configuration
Take a look into the file `mods/MilitaryMissions/config/MilitaryMissionsConfig.lua`. There some settings in the Config section to balance the pirate warlord mission.

### Optional: Activate XsotanDreadnought
If you are using the [XsotanDreadnought](http://www.avorion.net/forum/index.php/topic,4325.msg22852.html) mod you can use it as plugin for the military outpost. Just open the config file (path above). Find the XsotanDreadnought settings in Missions section and remove the leading "--". The comments in the file will help you.
Requires XsotanDreadnought 0.3.0 or higher!


## Add new missions
To add a new mission you need 2 files: `yourmission.lua` and `yourmissionBulletin.lua`. Replace "yourmission" by the mission name. 

It does not matter where you place these files, but you should keep file structure. I recommand to create a new mod directory: `mods/yourmission/scripts/player/missions/`.


Add the path to the bulletin file below the "Custom missions below" comment. The syntax is like this:

    {bulletin = "mods/yourmission/scripts/player/missions/yourmissionBulletin"},

Important is not to change any signs of that line, only the path and name of your file. Also you must NOT add the .lua part of the filename.

##### Hint: for testing enable dev mode of your test galaxy - this makes bulletin update every 5 seconds.

### Optional: Configuration for missions

For this step you should watch config file.

All vanilla missions, the Pirate Warlord and XsotanDreadnought are already configured correctly.

* bulletin = script to bulletin file without lua ending. Required!

You can set optional configurations for every mission:

* probability = % chance that outposts offer this missin. Values: 0-100. Leave blank to use 100%
* maxDistance = The mission will not be offered when distance to core is below this. Leave blank for not limiting.

Example for using optional values (50% chance to find, max distance from core 250):

    {bulletin = "mods/yourmission/scripts/player/missions/yourmissionBulletin", probability = 50, maxDistance = 250},


## Create a new mission
Guide for craeting new missions may come sooner or later. You may use Pirate Warlord Plugin as axample. Taking a look into some vanilla missions or events may also help.
The XsotanDreadnought can also help, but because it is both, random event and a mission in one script, it's too complicated to be a good example. 

## Compatibility

This mod overwrites the script of the military outpost, so it should not be compatible with any mod that overwrites the same file. If you already got a modded mission for military outpost you may publish a plugin for this mod to add it to the game.

## Credits

The mod itself, and especially the included mission "Pirate Warlord" uses some code snippets from ["Pirate Warlord" mod by Theoman02](http://www.avorion.net/forum/index.php/topic,781.0.html).

This mission was designed and coded by Theoman02. I converted it to be a plugin for this mod and and updated for current Avorion version.

## Roadmap

Make this mod independent from military outpost, so that it can be added to any type of station. Each station can have its own mission types. This would highly improve compatibility and flexibility of the mod.