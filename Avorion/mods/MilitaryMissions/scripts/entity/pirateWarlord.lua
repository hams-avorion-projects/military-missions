package.path = package.path .. ";mods/MilitaryMissions/?.lua"

local Config = require("config/MilitaryMissionsConfig")

local boss
local initialized

-- Don't remove or alter the following comment, it tells the game the namespace this script lives in. If you remove it, the script will break.
-- namespace PirateWarlord
PirateWarlord = {}

function PirateWarlord.initialize()
	boss = Entity()
	
	if Config.Settings.Warlord.enhanceShields and boss.maxDurability > boss.shieldMaxDurability then
		-- Increase shields, it should not be too easy :)
		local bonus = (boss.maxDurability / boss.shieldMaxDurability) * (math.random() * 0.4 + 0.8) -- Random multiplier between 0.8 and 1.2
		Config.log(4, "Multiply shields by "..bonus)
		boss:addKeyedMultiplier(StatsBonuses.ShieldDurability, 99999999, bonus)
	end
	
	boss.shieldDurability = boss.shieldMaxDurability
end
if onClient() then
function PirateWarlord.getUpdateInterval()
    return 0.1
end
function PirateWarlord.updateClient(timestep)
	-- Bad thing that this does not work for client on initializing... should be improved in future
	if not initialized then
		initialized = true
		boss.shieldDurability = boss.shieldMaxDurability
	end
	registerBoss(boss.index)
end
end

