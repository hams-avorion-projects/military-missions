# Changelog

### 0.3.4
* Bugfixing
* Improved performance of military outpost
* Improved mod config

### 0.3.3
* Fixed an issue wich threw out a harmless error when a mission bulletin could not be loaded

### 0.3.2
* Fixed an issue wich may broke the mod on linux. Should work on all OS now

### 0.3.1
* Initial release